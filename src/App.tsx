import React, { useState } from 'react';
import logo from './logo.svg';
import './App.css';
import { Input, useMIDI, useMIDINotes } from '@react-midi/hooks';

function App() {
  const { inputs } = useMIDI()
  const [getId, setId] = useState<string>();
  const input = getId != null ? inputs.find(n => n.id == getId) : null;
  return (
    <div className="App">
      <header className="App-header">
                <select onChange={n => { setId(n.currentTarget.value) }}>{[<option key="none">Select Device</option>,...inputs.map(n => <option key={n.id} value={n.id} >{n.name}</option>)]}</select>
        { !inputs.length ?
          <div>No Midi devices</div> :
            !input ? <div> No Input selected</div> :
              (<div>
                <br />
          Playing { input.name}
                <MIDINoteLog input={input} /> </div>) }
        <img src={logo} className="App-logo" alt="logo" />


      </header>
    </div>

  );
}

const MIDINoteLog = ({ input }: { input: Input }) => {
  const notes = useMIDINotes(input);
  return (
    <div>
      Playing notes: <ul> {!notes.length ? <li key="none">"N/A"</li> : notes.map(n => <li key={n.note}>`note: {n.note} velocity: {n.velocity} channel: {n.channel}`</li>)} </ul>
    </div>
  )
}

export default App;
